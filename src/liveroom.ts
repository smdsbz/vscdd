import * as vscode from 'vscode';
const puppeteer = require('puppeteer-core');
// import * as puppeteer from 'puppeteer-core';
import * as fs from 'fs';
import * as biliapi from './api';
import * as utils from './utils';
import { FollowsItem } from './followings';


let opened_liverooms = new Map<number, LiveRoom>();

class LiveRoom {
  disposables: vscode.Disposable[];
  /** Chrome executable path */
  chrome: string;
  quality: number;

  uid?: number;
  uname?: string;
  roomid?: number;

  /** 直播间标题 */
  title?: string;
  /** 推流地址 */
  url?: string;

  /** 视图实例 */
  view?: vscode.WebviewPanel;

  /**
   * @throws {Error}
   *    * `no valid Chrome` - 无 Chrome 程序配置
   */
  constructor(disposables: vscode.Disposable[]) {
    this.disposables = disposables;
    const chrome = vscode.workspace.getConfiguration('vscdd').get<string>('chromeExecutable');
    if (typeof chrome === 'undefined' || !fs.existsSync(chrome))
      throw new Error('no valid Chrome');
    this.chrome = chrome;
    this.quality = vscode.workspace.getConfiguration('vscdd').get<number>('renderQuality')!;
  }

  /**
   * @constructor
   * @async
   * @param [uid]
   * @param [roomid]
   * @returns new instance or existing, MUST BE CAUGHT!!!!
   * @throws {Error}
   *    * `stricted` - 会限直播
   *    * `non liver` - 该用户还未启用直播间
   *    * other
   */
  async init(uid?: number, roomid?: number): Promise<LiveRoom> {
    if (typeof uid === 'undefined' && typeof roomid === 'undefined')
      throw new Error('one of uid, roomid must be given');
    if (typeof uid === 'undefined')
      uid = await biliapi.get_uid_of_room(roomid!);
    this.uid = uid;
    if (typeof roomid === 'undefined')
      roomid = await biliapi.get_room_of_uid(this.uid);
    this.roomid = roomid;
    if (typeof this.roomid === 'undefined')
      throw new Error('non liver');
    // if already exist, return the existing
    if (opened_liverooms.has(this.roomid))
      return opened_liverooms.get(this.roomid)!;
    if (await biliapi.is_room_stricted(this.roomid))
      throw new Error('stricted');
    this.uname = await biliapi.get_uname_of_uid(this.uid);
    this.title = await biliapi.get_title_of_room(this.roomid);
    return this;
  }

  _get_ui_html(): string {
    return `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="padding: 0;">
  <img id="live-view" style="width: 100%;">
  <div><span>Volume: </span>
  <input id="volume-roker" type="range"
    min="0" max="1" step=".05" value=".3"
    onchange="changeVolume(this)">
  </div>

  <script>
const vscode = acquireVsCodeApi();
const live_view = document.querySelector('img#live-view');

/* live video rendering */
window.addEventListener('message', ev => {
  const msg = ev.data;
  switch (msg.cmd) {
    case 'refresh-live-view':
      live_view.src = msg.imguri;
      break;
    default:
      break;
  }
});

/* volume control */
function changeVolume(self) {
  vscode.postMessage({cmd: 'change-volume', value: self.value});
}
  </script>
</body>
</html>`;
  }

  /**
   * @deprecated During dev, Bilibili has blocked download requests to 
   */
  _get_render_html(): string {
    return `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Access-Control-Allow-Origin" content="*">
  <script src="https://cdn.bootcdn.net/ajax/libs/hls.js/1.0.1/hls.min.js"></script>
  <script src="https://cdn.bootcdn.net/ajax/libs/dplayer/1.26.0/DPlayer.min.js"></script>
</head>
<body style="padding: 0;">
  <p id="dbg"></p>
  <div id="live-player"></div>

  <script>
function onReady(cb) {
  if (document.readyState === "complete")
    cb();
  else
    window.addEventListener("load", cb);
}

console.log(document.readyState);
onReady(() => console.log(document.readyState));

onReady(() => {
  let display = new DPlayer({
  container: document.querySelector("div#live-player"),
  autoplay: true,
  live: true,
  video: {
    url: "https://d1--cn-gotcha105.bilivideo.com/live-bvc/558836/live_2370062_9705826.m3u8?cdn=cn-gotcha05&expires=1620115177&len=0&oi=804332725&pt=h5&qn=10000&trid=ac92ff0cdbd04a63a09d82f331174a6a&sigparams=cdn,expires,len,oi,pt,qn,trid&sign=fcf0ac6ef3b7bd64b53f32c74982140c&ptype=0&src=9&sl=2&sk=59b4112a8c653bb&order=1",
    type: "hls"
  }
  });
});
  </script>
</body>
</html>`;
  }

  async start_render() {
    if (typeof this.view !== 'undefined') {
      this.view.reveal();
      return this.view;
    }
    // create WebView
    this.view = vscode.window.createWebviewPanel(
      'vscdd.liveroom', `${this.title} - ${this.uname}`,
      {viewColumn: vscode.ViewColumn.Beside},
      {enableScripts: true, retainContextWhenHidden: true}
    );
    opened_liverooms.set(this.roomid!, this);
    this.view.onDidDispose(
      () => { opened_liverooms.delete(this.roomid!); },
      this, this.disposables
    );
    this.view.webview.html = this._get_ui_html();
    // create live render backend
    let backend = await puppeteer.launch({
      executablePath: this.chrome,
      ignoreDefaultArgs: ['--mute-audio'],
      /* DEBUG: */
      headless: true, devtools: true,
    });
    let is_closing_render = false;
    this.view.onDidDispose(async () => {await backend.close();}, this, this.disposables);
    backend.on('targetdestroyed', (t: any) => {is_closing_render = true;});
    let [live_page] = await backend.pages();
    await live_page.goto(`https://live.bilibili.com/${this.roomid}`);
    const fn_hide_element = async (selector: string) => {
      try {
        const el = await live_page.waitForSelector(selector);
        await live_page.evaluate((el: any) => {el.style.display='none'}, el);
        await el.dispose();
      }
      catch (e) {
        console.debug(e);
      }
    };
    // put away your dear Haruna :)
    await fn_hide_element('div#my-dear-haruna-vm');
    // get rid of sidebar
    await fn_hide_element('div#sidebar-vm');
    // don't show overflow danmaku, I have plans of my own
    await fn_hide_element('div.web-player-danmaku');
    // don't show other injects
    await fn_hide_element('div.web-player-inject-wrap');
    // register controls
    const fn_set_volume = async (vol: number) => {
      if (vol > 1) vol = 1;
      if (vol < 0) vol = 0;
      let video: any;
      try {
        // I dunno 'bout u, but my computer really need this 2 secs ...
        video = await live_page.waitForSelector('video', {timeout: 2000});
      }
      catch (e) {
        return;
      }
      await live_page.evaluate((vid: any, vol: number) => {vid.volume = vol}, video, vol);
      await video.dispose();
    }
    fn_set_volume(.3);
    this.view.webview.onDidReceiveMessage(
      async (msg: any) => {
        switch (msg.cmd) {
          case 'change-volume': fn_set_volume(msg.value); break;
          default: break;
        }
      },
      undefined, this.disposables
    );
    // get video viewport
    const player_mounter = await live_page.waitForSelector('div.live-player-mounter');
    try {
      while (!is_closing_render) {
        const screenshoturi = 'data:image/jpeg;base64,'
            + await player_mounter.screenshot({
                encoding: 'base64', type: 'jpeg', quality: this.quality});
        await this.view.webview.postMessage({
          cmd: 'refresh-live-view', imguri: screenshoturi
        });
      }
    }
    finally {
      player_mounter.dispose();
    }
  }
}


export async function createLiveRoom(disposables: vscode.Disposable[], roomid?: number) {
  let room = new LiveRoom(disposables);
  let uid: number | undefined = undefined;
  if (typeof roomid === 'undefined') {
    let follows = vscode.workspace.getConfiguration('vscdd').get<FollowsItem[]>('follows')!;
    let picks: (vscode.QuickPickItem & {uid?: number, roomid?: number})[] = [];
    for (let f of follows)
      picks.push({ label: `${f.uname}`, description: `UID: ${f.uid}`,
                   detail: f.hint ?? '',
                   uid: f.uid, roomid: f.room_id});
    const picked = await vscode.window.showQuickPick(
        picks, {matchOnDescription: true, matchOnDetail: true});
    if (typeof picked === 'undefined')
      return;
    uid = picked.uid;
    roomid = picked.roomid;
  }
  room = await room.init(uid, roomid);
  await room.start_render();
}
