vscdd
=====

<center><b>Viva eSprite of 臭 「誰でも大好き」</b></center>

<br />

VS Code DD 小助手，帮助你在摸鱼的时候更好地工作 :pray:

| ![](Documents/assets/vscdd.gif) |
|:-------------------------------:|
|       在我的渣渣机器上的效果        |


-----------------------------------------------------------------------

Features
--------

* [x] 开播提醒 `snooping`

    <center><small><i>幸福往往是摸得透彻，而堇业的心却常常隐藏。</i></small></center>

    * [x] 后台监控
    * [x] 监控列表设置项 `vscdd.follows`
        * [x] 自动从 B 站同步 / 拉取关注列表

* [ ] CustomView 直播播放器


Requirements
------------

一颗兼济天下的心 :heart: (笑)


Extension Settings
------------------

摸了，见设置页面。

Known Issues
------------

_Dog-fooding ..._

_File issues on [Gitee](https://gitee.com/smdsbz/vscdd)._


Release Notes & Roadmap
-----------------------

#### 0.0.1

* static live status monitoring

#### 0.0.2

* command to automagically merge follow list to monitor list

### 0.1.0

* open liveroom in VSCode as WebView


---------------------------------------------------------------------------

<details>
<summary>About me</summary>

* Bilibili: [神马都是包子](https://space.bilibili.com/3652858)
* GitHub: [smdsbz](https://github.com/smdsbz)
* Gitee: [smdsbz](https://gitee.com/smdsbz)
</details>
